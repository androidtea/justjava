package com.example.android.justjava;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    int     quantity = 0;
    int     basePrice = 5;
    int     creamPrice = 1;
    int     chocolatePrice = 2;
    String customerName = "";
    boolean isWithCream = false;
    boolean isWithChocolate = false;
    CheckBox is_with_cream_box;
    CheckBox is_with_chocolate_box;
    EditText customer_name;
    TextView quantityTextView;
    TextView orderSummaryTextView;
    Toast notifyMinToast;
    Toast notifyMaxToast;
    Context context;
    int duration = Toast.LENGTH_SHORT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
        is_with_cream_box       = (CheckBox) findViewById(R.id.is_with_cream);
        is_with_chocolate_box   = (CheckBox) findViewById(R.id.is_with_chocolate);
        customer_name       = (EditText) findViewById(R.id.customer_name);
        context             = getApplicationContext();
        notifyMinToast      = Toast.makeText(this, getString(R.string.notify_min_toast), duration);
        notifyMaxToast      = Toast.makeText(this, getString(R.string.notify_max_toast), duration);
//        quantityTextView    = (TextView) findViewById(R.id.quantity_text_view);
    }

    /**
     * This method increments the given quantity value on the screen.
     */
    public void increment(View view) {
        if (quantity < 100) {
            quantity++;
        } else {
            notifyMaxToast.show();
        }
        displayQuantity(quantity);
    }

    /**
     * This method decrements the given quantity value on the screen.
     */
    public void decrement(View view) {
        if (quantity > 0) {
            quantity--;
        } else {
            notifyMinToast.show();
        }
        displayQuantity(quantity);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        isWithCream     = is_with_cream_box.isChecked();
        isWithChocolate = is_with_chocolate_box.isChecked();
        customerName    = customer_name.getText().toString();
//        displayMessage(createOrderSummary(calculatePrice(quantity, basePrice)));
        Intent emailIntent = new Intent(
                Intent.ACTION_SENDTO,
                Uri.fromParts(
                    getString(R.string.email_mail_to_text),
                    getString(R.string.email_address),
                    null
                )
            );
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject, customerName));
        emailIntent.putExtra(Intent.EXTRA_TEXT, createOrderSummary(calculatePrice(quantity, basePrice)));
        if (emailIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(emailIntent, getString(R.string.email_send_text)));
        }
    }

    /**
     *
     * @param totalPrice price
     * @return string text with order summary
     */
    public String createOrderSummary(int totalPrice) {
        String priceMessage = getString(R.string.summary_name, customerName);
        priceMessage += getString(R.string.summary_cream, isWithCream);
        priceMessage += getString(R.string.summary_chocolate, isWithChocolate);
        priceMessage += getString(R.string.summary_quantity, quantity);
        priceMessage += getString(R.string.summary_total, NumberFormat.getCurrencyInstance().format(totalPrice));
        priceMessage += getString(R.string.summary_thank_you);

        return priceMessage;
    }

    /**
     *
     * @param quantity of
     * @param price price
     * @return int
     */
    private int calculatePrice(int quantity, int price) {
        if (isWithCream) {
            price += creamPrice;
        }

        if (isWithChocolate) {
            price += chocolatePrice;
        }
        return quantity * price;
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity(int numberOfCoffees) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + numberOfCoffees);
    }

//    /**
//     * This method displays the given text on the screen.
//     */
//    private void displayMessage(String message) {
//        orderSummaryTextView.setText(message);
//    }

}